﻿using System;
using System.Collections.Generic;
using System.Text;
using Bogus;
using NSubstitute;
using Shouldly;
using SimpleTTD.Logic;
using SimpleTTD.Logic.Exceptions;
using Xunit;

namespace SimpleTTD.Tests.FerrariStore_Test
{
    public class When_registering_shipped_car
    {
        private FerrariStore _ferrariStore;
        private RegistredCar _registredCar;
        private Faker<Car> _carFaker =new Faker<Car>();
        private ICarRepository _repository;
        private Car GetRandomCar(string vin = null)
        {
            Bogus.Faker faker = new Faker("pl");
            Faker<Car> carFaker = new Faker<Car>("pl");
            int inc = 0;
            carFaker.RuleFor(c => c.Vin, (f, c) => vin ?? "VIN" + faker.Random.Number(100))
                .RuleFor(c => c.Cost, (f, c) => f.Finance.Amount(10))
                .RuleFor(c => c.DealerPrice, (f, c) => c.Cost + f.Finance.Amount(1, 30))
                .RuleFor(c => c.Model, (f, c) => f.PickRandom(new[] { "250", "288 GTO", "F40", "599" }))
                .RuleFor(c => c.OnlinePrice, (f, c) => c.Cost + f.Finance.Amount(1, 30));

            return carFaker.Generate();

        }

        private IEnumerable<RegistredCar> GetRegistredCars(int count)
        {
            List<RegistredCar> registredCars=new List<RegistredCar>();
            for (int i = 0; i < count; i++)
            {
                registredCars.Add(new RegistredCar(GetRandomCar(),Guid.NewGuid()));
            }

            return registredCars;
        }

        public When_registering_shipped_car()
        {
            _repository = Substitute.For<ICarRepository>();
            _ferrariStore = new FerrariStore(_repository);
        }


        [Fact]
        public void that_is_null_should_throw_NeedCarInformation()
        {
            Car car = null;
            Should.Throw<NeedCarInformation>(() => _ferrariStore.RegisterShippedCar(car));
        }

        public void Arrange_that_already_is_register_return_that_information_with_id(Guid registeredCarId,Car registredCar)
        {
            _repository = Substitute.For<ICarRepository>();
            _repository.When(c => c.Add(Arg.Any<RegistredCar>())).Do(c => { });

            _repository.GetBy(Arg.Is<string>(s => s == registredCar.Vin))
                .Returns(c => new[] {new RegistredCar(registredCar, registeredCarId)});
            _ferrariStore=new FerrariStore(_repository);
        }

        [Fact]
        public void that_already_is_register_return_that_information_with_id()
        {
            Guid registredId = Guid.NewGuid();
            Car registredCar = GetRandomCar();
            Arrange_that_already_is_register_return_that_information_with_id(registredId,registredCar);
            _ferrariStore.RegisterShippedCar(registredCar);
            var result= _ferrariStore.RegisterShippedCar(registredCar);

            result.Id.ShouldBe(registredId);
            result.IsAlreadyRegistred.ShouldBeTrue();
        }
        [Fact]
        public void that_is_not_in_system_add_it_to_sytem_and_return_id()
        {


            var result = _ferrariStore.RegisterShippedCar(GetRandomCar());
            result.Id.ShouldNotBe(Guid.Empty);
            result.IsAlreadyRegistred.ShouldBeFalse();

        }

        [Fact]
        public void that_is_not_in_system_will_fire_add_in_repository()
        {
            _ferrariStore.RegisterShippedCar(GetRandomCar());
            _repository.Received(1).Add(Arg.Any<RegistredCar>());
        }


        public void Arrange_that_is_in_system_will_not_fire_add_method(Guid registeredCarId, Car registredCar)
        {
            _repository = Substitute.For<ICarRepository>();
            _repository.When(c => c.Add(Arg.Any<RegistredCar>())).Do(c => { });

            _repository.GetBy(Arg.Is<string>(s => s == registredCar.Vin))
                .Returns(c => new[] { new RegistredCar(registredCar, registeredCarId) });
            _ferrariStore = new FerrariStore(_repository);
        }
        [Fact]
        public void that_is_in_system_will_not_fire_add_method()
        {
            Guid registredId = Guid.NewGuid();
            Car registredCar = GetRandomCar();
            Arrange_that_is_in_system_will_not_fire_add_method(registredId, registredCar);
            _ferrariStore.RegisterShippedCar(registredCar);
            _repository.DidNotReceive().Add(Arg.Any<RegistredCar>());
        }
        [Fact]
        public void is_not_null_should_fire_get_method_by_vin()
        {
            _ferrariStore.RegisterShippedCar(GetRandomCar());
            _repository.Received(1).GetBy(Arg.Any<string>());
        }
    }
}
