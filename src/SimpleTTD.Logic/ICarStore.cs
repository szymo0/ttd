﻿using System;

namespace SimpleTTD.Logic
{
    public interface ICarStore
    {
        RegistredShippedCarResult RegisterShippedCar(Car car);

        Summary GetStoreSummary();

    }


    public class Summary
    {
        public int RegistredShippedCar { get; set; }
        public DateTime CreationDate { get; set; }
    }

    public class RegistredShippedCarResult
    {
        public Guid Id { get; set; }
        public bool IsAlreadyRegistred { get; set; }
    }
}