﻿using System.Collections.Generic;

namespace SimpleTTD.Logic
{
    public interface ICarRepository
    {
        IEnumerable<RegistredCar> GetBy(string vin);
        void Add(RegistredCar registredCar);
    }
}