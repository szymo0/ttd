﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleTTD.Logic.Exceptions;

namespace SimpleTTD.Logic
{
    public class FerrariStore: ICarStore
    {
        private readonly ICarRepository _repository;

        public FerrariStore(ICarRepository repository)
        {
            _repository = repository;
        }
        public RegistredShippedCarResult RegisterShippedCar(Car car)
        {
            if (car == null) throw new NeedCarInformation();
            var registredCar = _repository.GetBy(car.Vin).FirstOrDefault();
            if (registredCar==null)
            {
                var newCar = new RegistredCar(car);
                
                _repository.Add(newCar);

                return new RegistredShippedCarResult()
                {
                    Id = newCar.Id,
                    IsAlreadyRegistred = false
                };
                
            }

            return new RegistredShippedCarResult
            {
                Id = registredCar.Id,
                IsAlreadyRegistred = true
            };
        }

        public Summary GetStoreSummary()
        {
            throw new NotImplementedException();
        }
    }





}
