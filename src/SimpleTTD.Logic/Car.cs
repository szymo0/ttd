﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleTTD.Logic
{
    public class Car
    {
        public string Vin { get; set; }
        public string Model { get; set; }
        public decimal DealerPrice { get; set; }
        public decimal OnlinePrice { get; set; }
        public decimal Cost { get; set; }
    }

    public class TestCar : Car
    {
        public bool IsReserved { get; set; }
        public DateTime? ReservedFrom { get; set; }
        public DateTime? ReservedTo { get; set; }
        public string ReservedBy { get; set; }
        public decimal OdometerStartValue { get; set; }
        public decimal OdomoterOldValue { get; set; }
    }

    public class RegistredCar
    {
        public Guid Id { get; private set; }
        public Car Car { get; private set; }
        public RegistredCar(Car car, Guid id)
        {
            Car = car;
            Id = id;
        }

        public RegistredCar(Car car)
        {
            Car = car;
            Id=Guid.NewGuid();
        }
    }
}
